const themeSetting = {
    fontWeights: {},
    durations: {},
    borderWidths: {},
    colors: {},
    fontSizes: {},
    fontFamilies: {
      montserrat: "Montserrat",
      merriweather: "Merriweather",
      openSans: "Open Sans",
    },
    letterSpacings: {},
    lineHeights: {},
    mediaQueries: {},
    opacities: {},
    radii: {},
    shadows: {},
    spacing: {},
    zIndices: {},
  };
  
  module.exports = themeSetting;
  